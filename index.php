<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");

echo 'Nama Binatang : ' . $sheep -> name . '<br>'; // "shaun"
echo 'Jumlah Kaki : ' . $sheep -> legs . '<br>'; // 2
echo 'Hewan Darah Dingin : ' . $sheep -> cold_blooded . '<br><br>'; // false

$kodok = new Frog("Buduk");
echo 'Nama Binatang : ' . $kodok -> name . '<br>';
echo 'Jumlah Kaki : ' . $kodok -> legs . '<br>';
echo 'Hewan Darah Dingin : ' . $kodok -> cold_blooded . '<br><br>';
$kodok->jump(); // "hop hop"

$sungokong = new Ape("kera sakti");
echo 'Nama Binatang : ' . $sungokong -> name . '<br>';
echo 'Jumlah Kaki : ' . $sungokong -> legs . '<br>';
echo 'Hewan Darah Dingin : ' . $sungokong -> cold_blooded . '<br>';
$sungokong->yell(); // "Auooo"