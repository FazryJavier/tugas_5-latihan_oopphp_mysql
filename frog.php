<?php

require_once('animal.php');

    class Frog extends Animal {
        public $legs = 4;
        public $cold_blooded = "True";
        public $sound = "Hop Hop";

        function jump() {
            echo 'Suara Melompat : ' . $this -> sound . '<br>';
        }
    }