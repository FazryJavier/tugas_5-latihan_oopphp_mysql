Soal 1 Membuat Database
Buatlah database dengan nama “myshop”. Tulislah di text jawaban pada nomor 1.

CREATE DATABASE myshop;

---------------------------------------------------------------------------------------------------------------------

Soal 2 Membuat Table di Dalam Database
Buatlah tabel – tabel baru di dalam database myshop sesuai data-data berikut.

Tabel User :
CREATE TABLE users(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    email varchar(255) NOT null,
    password varchar(255) NOT null
    );

Tabel Kategori :    
CREATE TABLE categories(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null
    );

Tabel Barang : 
CREATE TABLE items(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    description varchar(255),
    price int NOT null,
    stock int NOT null,
    categories_id int(8),
    FOREIGN KEY(categories_id) REFERENCES categories(id)
);

---------------------------------------------------------------------------------------------------------------------

Soal 3 Memasukkan Data pada Table
Masukkanlah data data berikut dengan perintah SQL “INSERT INTO . . ” ke dalam table yang sudah dibuat pada soal sebelumnya.

Insert Data User :
INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

Insert Data Kategori :
INSERT INTO categories(name) VALUES ("Gadget"), ("Cloth"), ("Men"), ("Women"), ("Branded");

Insert Data Barang :
INSERT INTO items(name, description, price, stock, categories_id) VALUES ("Sumsang B50", "Hape keren dari merek Sumsang", 4000000, 100, 1), ("Uniklooh", "Baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "Jam tangan anak yang jujur banget", 2000000, 10, 1);

---------------------------------------------------------------------------------------------------------------------

Soal 4 Mengambil Data dari Database
a. Mengambil data users
Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.

SELECT id, name, email FROM users;


b. Mengambil data items
- Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

SELECT * FROM items WHERE price > 1000000;


- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM items WHERE name LIKE "%sang%";


c. Menampilkan data items join dengan kategori
Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items (gunakan join).

SELECT items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name AS kategori FROM items INNER JOIN categories on items.categories_id = categories.id;

---------------------------------------------------------------------------------------------------------------------

Soal 5 Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000.

UPDATE items SET price = 2500000 where id = 1;
